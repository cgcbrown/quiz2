<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('form', function () {
	return view('car_form');
});

Route::post('form', 'CarController@post')->name('car.post');

Route::get('animal/{cat}/price/{value}', 'ThingController@something')->('animal.stuff');

Route::get('grid', function () {
	return view('css10');
});